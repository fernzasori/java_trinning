# Java_Trinning
Import File WebTier And AppTier to Project  From  WebLogic Workshop
#1) Java Programming  
How to Learning Java Source Code in Project WebTier -> At Package th.co.extosoft.trainning
#You can Learn Java Source Code As follows.
ArrayList   --> trainning.TestArrayList.java
ConnectDB   --> trainning.TestConnectDB.java
DateFormat  --> trainning.TestDateFormat.java
ForLoop   --> trainning.TestForLoop.java
HashMap     --> trainning.TestHashMap.java
IfElse      --> trainning.TestIfElse.java
TestNumber  --> trainning.TestNumber.java
ReadProperties   --> trainning.TestReadProperties.java
String   --> trainning.TestString.java
Vector   --> trainning.TestVector.java

2) Java connect to MySQL  
    Install MySQL And Set locall 7001
    For connect to MySQL by WebLogic Workshop

3) Create Java Web Service (API) with Method GET, POST, ADD , UPDATE, DELETE
   Create by WebTierTest.java  At Package --> th.co.extosoft.web.service
   
4) Create JSON call Web Service (API)
    URL Backend : http://localhost:7001/WebTier/WebTierTest?Add 
    URL Call to JSON : http://jsonviewer.stack.hu/

5) How to deploy .war on Tomcat


6) Setup Web Tier and App Tier
#WebTier Set ...
    ConfigurationRequest --> th.co.extosoft.app.biz.AppTierBusiness.java
    Database credentials --> th.co.extosoft.app.db.AppTierDB.java
    AppTierConfigInit    --> th.co.extosoft.app.init.AppTierConfigInit.java
                         --> th.co.extosoft.app.init.AppTierLog4jInit.java
    ConfigurationRequest (get, set ,delete) --> th.co.extosoft.app.itf.AppTierResponse.java
                                            --> th.co.extosoft.app.itf.ConfigurationRequest.java
    ConfigurationResponse --> th.co.extosoft.app.itf.ConfigurationResponse.java
    AppTierWS (WebService) -->  th.co.extosoft.app.ws.AppTierWS.java
#AppTier Set ...
 WebService  -->  th.co.extosoft.web.service.WebTierTest.java
App Flow: Rest Full JSON -> Web Tier -> App Tier (Web Service) -> MySQL DB


